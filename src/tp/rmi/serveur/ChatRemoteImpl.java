package tp.rmi.serveur;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;

import tp.rmi.common.ChatRemote;
import tp.rmi.common.ReceiveCallback;

public class ChatRemoteImpl extends UnicastRemoteObject implements ChatRemote, Remote, Serializable {




	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<ReceiveCallback> callbacks;
	
	protected ChatRemoteImpl() throws RemoteException {
		super();
	}

	@Override
	public String echo(String name, String message) {
		return name+">"+message;
	}

	@Override
	public void send(String name, String message) throws RemoteException {
		for(ReceiveCallback  c : callbacks)
		{
			c.newMessage(echo(name,message));
		}
	}

	@Override
	public void registerCallback(ReceiveCallback callback) throws RemoteException {
		callbacks.add(callback);
		
	}

}
